﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalXML
{
    public static class Oracle
    {
        public static readonly Random Random = new Random();
        public static readonly Dictionary<int, string> FemaleNames = new Dictionary<int, string>()
        {
            {1, "Agata"}, {2, "Batty"}, {3, "Jessie"}, {4, "Caroline"}, {5, "Leona"}, {6, "Josephine"}, {7, "Melissa"}, {8, "Victoria"}, {9, "Sophia"}
        };
        public static readonly Dictionary<int, string> MaleNames = new Dictionary<int, string>()
        {
            {1, "James"}, {2, "David"}, {3, "Ronald"}, {4, "Andrew"}, {5, "Roger"}, {6, "Gavin" }, {7, "Howard"}, {8, "Jose"}, {9, "Neil"}
        };
        public static Guid GetGuid() => Guid.NewGuid();

        private static string GetMaleName() => MaleNames[Random.Next(1, MaleNames.Count + 1)];
        private static string GetFemaleName() => FemaleNames[Random.Next(1, FemaleNames.Count + 1)];
        public static string GetName(BirdSex birdSex)
        {
            return birdSex == BirdSex.Female
                ? Oracle.GetFemaleName()
                : Oracle.GetMaleName();
        }
    }
}
