﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalXML
{
    public enum BirdFamilyStatus
    {
        Husband,
        Wife,
        Son,
        Daughter,
        Grandpa,
        Great_grandfather,
        Great_great_grandfather,
        Grandmother,
        Great_grandmother,
        Great_great_grandmother
    }
}
