﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalXML
{
    public class Bird
    {
        public Bird()
        {
        }
        public Bird(string name, BirdSex sex)
        {
            Sex = sex;
            Name = name;

            Color = (ConsoleColor)Oracle.Random.Next(1, 16);
        }
        /// <summary> Птичка на выбор (усыновление). </summary>
        public Bird(BirdSex sex, ConsoleColor color)
        {
            Sex = sex;
            Name = Oracle.GetName(sex);
            Color = color;
        }
        public Guid Id = Oracle.GetGuid();
        public DateTime BirthDate = DateTime.UtcNow;

        public string Name;
        public ConsoleColor Color;

        public BirdSex Sex;
        //public List<Bird> Children { get; set; } = new List<Bird>();

        public FamilyStatus FamilyStatus;
        //public List<Bird> FamilyPartners { get; set; } = new List<Bird>();
        public Guid PartnerId;
    }
}
