﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace LocalXML
{
    public class LocalXmlReader : IEnumerable<Bird>, IDisposable
    {
        private readonly Stream _stream;
        private readonly List<Bird> _collection = new List<Bird>();
        private readonly Algorithm _algorithm = new Algorithm();
        public LocalXmlReader(Stream stream, IAlgorithm algorithm)
        {
            _stream = stream ?? throw new ArgumentNullException(nameof(stream));
            using var reader = new StreamReader(_stream);
            var xml = reader.ReadToEnd();

            if (string.IsNullOrEmpty(xml))
            { throw new ArgumentNullException(); }

            var serializer = new ConfigurationContainer().ConfigureType<Bird>().Create();
            _collection = serializer.Deserialize<List<Bird>>(xml);
        }

        public IEnumerator<Bird> GetEnumerator()
        {
            return ((IEnumerable<Bird>)_collection).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        public void Dispose()
        {
            _stream.Dispose();
        }
    }
}
