﻿using System;
using System.Collections.Generic;
using System.IO;

namespace LocalXML
{
    class Program
    {
        static void Main(string[] args)
        {
            //=========================Создание XML=========================
            //var birds = new List<Bird>();
            //for (int i = 0; i < 10; i++)
            //{
            //    birds.Add(new Bird(Oracle.GetName(BirdSex.Male), BirdSex.Male));
            //    birds.Add(new Bird(Oracle.GetName(BirdSex.Female), BirdSex.Female));
            //}

            //IExtendedXmlSerializer serializer = new ConfigurationContainer().ConfigureType<Bird>().Create();
            ////IExtendedXmlSerializer serializer = new ConfigurationContainer().Create();
            //string contents = serializer.Serialize(birds);
            //File.WriteAllText("birds.xml", contents);
            //=========================Создание XML=========================
            Algorithm algorithm = new Algorithm();
            LocalXmlReader localXml;
            using (var stream = new FileStream("birds.xml", FileMode.Open))
            {
                localXml = new LocalXmlReader(stream, algorithm);
            }

            foreach (var item in localXml)
            {
                Console.WriteLine(item.Name);
            }

            Console.WriteLine($"Выполняется сортировка по длине строк.");

            foreach (var item in algorithm.SortByLength(localXml))
            {
                Console.WriteLine(item.Name);
            }

            Console.WriteLine($"Число символов в коллекции: {algorithm.CharsCountInBirdNames(localXml)}.");
            Console.WriteLine($"Число символов 'g' в коллекции: {algorithm.CharCountInBirdNames(localXml,'g')}.");

            Console.ReadLine();
        }
    }
}
