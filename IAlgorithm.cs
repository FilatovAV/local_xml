﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalXML
{
    public interface IAlgorithm
    {
        /// <summary> Общее число символов в коллекции данных. </summary>
        int CharsCountInBirdNames(IEnumerable<Bird> birds);
        /// <summary> Число заданных символов в коллекции данных. </summary>
        int CharCountInBirdNames(IEnumerable<Bird> birds, char chr);
        /// <summary> Сортировка по длине строк. </summary>
        IEnumerable<Bird> SortByLength(IEnumerable<Bird> birds);
    }
}
