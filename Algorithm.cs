﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LocalXML
{
    class Algorithm : IAlgorithm
    {
        public int CharCountInBirdNames(IEnumerable<Bird> birds, char chr)
        {
            return birds.Sum(s => s.Name.Count(c => c == chr));
        }

        public int CharsCountInBirdNames(IEnumerable<Bird> birds)
        {
            return birds.Where(w => !string.IsNullOrEmpty(w.Name)).Sum(s => s.Name.Length);
        }

        public IEnumerable<Bird> SortByLength(IEnumerable<Bird> bird)
        {
            return bird.OrderBy(b=> b.Name.Length);
        }
    }
}
