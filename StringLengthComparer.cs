﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace LocalXML
{
    /// <summary> Птички сравниваются по длине имени. =) </summary>
    internal class StringLengthComparer : IComparer<Bird>
    {
        public int Compare([AllowNull] Bird x, [AllowNull] Bird y)
        {
            if (string.IsNullOrEmpty(x.Name))
            {
                return string.IsNullOrEmpty(y.Name) ? 0 : -1;
            }

            if (string.IsNullOrEmpty(y.Name))
            {
                return 1;
            }

            var retval = x.Name.Length.CompareTo(y.Name.Length);
            return retval != 0 ? retval : x.Name.CompareTo(y.Name);
        }
    }
}
